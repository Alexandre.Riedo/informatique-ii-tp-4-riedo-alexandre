class priorityFile:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        itemPriority = item[0] # Je suppose que les items sont des tuples de la forme (indice, "nom")

        if self.is_empty() == True: # On ajoute de toute façon si la file est vide
            self.items.append(item)
        else:
            for i in range(0, len(self.items)): # On va parcourir la file d'attente
                otherItemPriority = self.items[i][0]

                if itemPriority > otherItemPriority: # On arrive sur l'indice où l'item a une priorité plus grande
                    self.items.insert(i, item)
                    break

                if itemPriority == otherItemPriority: # On arrive sur l'indice où l'item a la même priorité
                    self.items.insert(i, item)
                    break

                if i == (len(self.items)-1): # On arrive au bout de la file
                    self.items.append(item)
                    break

    def dequeue(self, deletionPriority):
        for i in range(0, len(self.items)): # On va parcourir la file
            otherItemPriority = self.items[i][0]

            if otherItemPriority < deletionPriority: # On arrive sur l'indice i du premier item avec une priorité plus importante
                if self.items[i-1][0] == deletionPriority: # On check si l'élément juste avant (i-1) est de la bonne priorité
                    return self.items.pop(i-1) # On enlève l'item
                break # On quitte de toute façon : soit on a enlevé l'item, soit les items d'avant ne sont pas de la bonne priorité
        
            if i == (len(self.items)-1) and otherItemPriority == deletionPriority: 
                # On arrive au bout de la liste : si le dernier item a la bonne priorité, on l'enlève
                return self.items.pop()

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

# Tests enqueue
testFile = priorityFile()
testFile.enqueue((4, "Nathan"))
testFile.enqueue((2, "Julia"))
testFile.enqueue((1, "Amandine"))
testFile.enqueue((3, "Mathias"))
testFile.enqueue((2, "Julia Jr."))
testFile.enqueue((4, "Nathan Jr."))
testFile.enqueue((1, "Amandine Jr."))
testFile.enqueue((1, "Amandine Sr."))
testFile.enqueue((1, "Amandin"))
print("Voici le résultat des tests enqueue() : ", testFile.items,"\n")

# Tests dequeue
print("Voici le résultat des tests de dequeue() : ")
print("On dequeue la priorité 4 et etc. : ", testFile.dequeue(4))
print(testFile.dequeue(2))
print(testFile.dequeue(2))
print(testFile.dequeue(2))
print(testFile.dequeue(1))
print("Voici notre file à ce stade : ", testFile.items)
testFile.dequeue(1)
testFile.dequeue(1)
testFile.dequeue(4)
testFile.dequeue(3)
print("Le tout dernier élément de la file à ce stade est : ", testFile.dequeue(1))
print("Voici notre file au final : ", testFile.items)